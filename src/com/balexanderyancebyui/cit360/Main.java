package com.balexanderyancebyui.cit360;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        try {
            collectionsExample();
        } catch (Exception e) {
            System.out.println("Something went wrong.\n" + e.toString());
        }
    }

    static void collectionsExample() {

        Random random = new Random();

        /************
         * List
         ************/
        ArrayList<Integer> list = new ArrayList<>();

        //for each of six iterations, count
        //each number twice 0-2
        for (int i = 0; i < 6; i++) list.add(i / 2);

        printTitle(list);
        printIterable(list);


        /************
         * Set
         ************/
        TreeSet<Integer> set = new TreeSet<>();

        //for each of six iterations, "try to" count
        //each number twice 0-2
        for (int i = 0; i < 6; i++) set.add(i / 2);

        printTitle(set);
        printIterable(set);

        /************
         * Queue
         ************/
        LinkedList<Integer> queue = new LinkedList<>();

        //for each of six iterations,
        //add a random int 0-4
        for (int i = 0; i < 6; i++) queue.add(random.nextInt(5));

        printTitle(queue);
        printIterable(queue);

        /************
         * Map
         ************/
        Map<String, Integer> map = new Hashtable<>();

        String key;
        int value;

        for (int i = 0; i < 6; i++) {
            key = String.format("%03d", random.nextInt(1000));
            value = random.nextInt(5);
            map.put(key, value);
        }

        printTitle(map);
        printKeyValue(new TreeSet<>(map.keySet()), map);
    }

    /***********
     * Print the Class of the given object
     ***********/
    static void printTitle(Object object) {
        System.out.printf("\n%s:\n", object.getClass().getSimpleName());
    }

    /***********
     * Print the contents of an iterable
     ***********/
    static void printIterable(Iterable<Integer> iterable) {
        for (Object i : iterable) {
            System.out.println(i.toString());
        }
    }

    /***********
     * Print the contents of a collection that has key/value pairs
     ***********/
    static void printKeyValue(TreeSet<String> set, Map<String, Integer> map) {
        for (Object i : set) {
            System.out.printf("%s: %d\n", i.toString(), map.get(i));
        }
    }
}
